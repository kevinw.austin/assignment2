from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    return {
   "books":[
      {
         "title":"Software Engineering",
         "id":"1"
      },
      {
         "title":"Algorithm Design",
         "id":"2"
      },
      {
         "title":"Python",
         "id":"3"
      }
   ]
}

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == "POST":
        newTitle = request.form['Title']
        books.append({'title': newTitle, 'id': str(len(books)+1)})
        return redirect(url_for("showBook"))
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == "POST":
        newTitle = request.form['editTitle']
        books[book_id-1]['title'] = newTitle
        return redirect(url_for("showBook"))
    else:
        return render_template('editBook.html', book = books[book_id-1])
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == "POST":
        del books[book_id-1]
        if len(books) > book_id-1:
            for book in books[book_id-1:]:
                book['id'] = str(int(book['id'])-1)
        return redirect(url_for("showBook"))
    else:
        return render_template('deleteBook.html', book = books[book_id-1])

if __name__ == '__main__':
	app.debug = True
	app.run()
	

